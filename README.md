# Portfolio (API)

Ceci est mon portfolio, il regroupera mon CV ainsi que mes différentes réalisations.

## Stack
**Back end :** PHP/Symfony \
**Front end :** React

## Environnement API

- PHP 8.3
- Symfony 6
- PostgreSQL 16
